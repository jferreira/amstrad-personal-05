# Amstrad Personal 05

Listados correspondientes a la [Amstrad Personal #5](https://archive.org/details/AmstradPersonal05/mode/2up)

- [Music I](https://archive.org/details/AmstradPersonal05/page/n77/mode/2up)
- [Missile Command](https://archive.org/details/AmstradPersonal05/page/n81/mode/2up)
- [Cargador Universal](https://archive.org/details/AmstradPersonal05/page/n21/mode/2up)

  El listado del cargador universal disponible en [cpcpower](https://www.cpc-power.com/index.php?page=detail&num=14204)
- [Graficos de tortuga en Basic](https://archive.org/details/AmstradPersonal05/page/n71/mode/2up)

## Como generar el dsk con los listados
```
$ make
```
Los listados .bas se aniaden al disco utilizado iDSK en formato texto.

La carga de estos es mas lenta que si el listado estuviera en formato basic.

Para convertir a formato basic (tokens)
```
load"programa.bas"
save"programa.bas"
```

## Como formatear los listados para verlos como en la revista
```
$ fold -w35 music.bas | more
```

## Graficos de tortuga en Basic
El listado basic **tortuga.bas** se genero a partir del fichero hexadecimal **tortuga.hex**.

Utilizado la utilidad cargador.py se puede comprobar en el PC la validez del fichero **tortuga.hex**.

La utilidad genera un fichero binario que se podria incluir en el disco utilizado iDSK.

Otra forma de generar el fichero binario es utilizando el Cargador Universal de Codigo Maquina:

- Ejecutar el Cargador Universal de Codigo Maquina **cargador.bas**
- Seleccionar la opcion **CARGAR BASIC** e introducir el nombre del fichero basic **tortuga.bas**
- Seleccionar la opcion para salvar en binario e introducir el nombre del fichero binario **tortuga.bin**
- Hacer un reset
- Para cargar los commandos RSX
```
memory &9fff:load"tortuga.bin",&a000:call &a000
```

## Missile Command

El listado contine una pequena rutina en codigo maquina
```
org #8000
ld l,(ix+#04)
ld h,(ix+#05)
ld e,(ix+#06)
ld d,(ix+#07)
call #bbc0
ld a,#04
call #bbde
ld a,#01
call #bc59
ld a,#2b
call #bbfc
ld l,(ix+#00)
ld h,(ix+#01)
ld e,(ix+#02)
ld d,(ix+#03)
call #b8c0
ld a,#2b
call #bbfc
ld a,#00
jp #bc59
```
