#!/usr/bin/env python3

import os
import sys
import argparse
from textwrap import wrap

def main():

    parser = argparse.ArgumentParser(description="Cargador Universal de Amstrad Personal",
                                     epilog="Written by Jesus Ferreira <jesus.ferreira@gmail.com>")

    parser.add_argument("-s", dest="start", help="Start address", type=int)
    parser.add_argument("-f", help="force")
    parser.add_argument("input")
    parser.add_argument("output")

    args = parser.parse_args()

    if not os.path.isfile(args.input):
        print("File path {} does not exist. Exiting...".format(args.input))
        sys.exit()

    output_bytes = bytearray()
    with open(args.input) as fp:
        for index, line in enumerate(fp):
            if line.strip():
                print("Line {}: {}".format(index + 1, line.strip()))
                if len(line.split(',')) != 2:
                    print("Wrong formatting.")
                    print("Line {}: {}".format(index + 1, line.strip()))
                    sys.exit()

                chunk_size = 2
                chunks = 12
                data = line.split(',')[0]
                checksum = int(line.split(',')[1], 16)

                byte_list = [ int(line[i:i+chunk_size],16) for i in range(0, len(data), chunk_size) ]

                if len(byte_list) != chunks:
                    print("Wrong data length at line {}.".format(index + 1))
                    sys.exit()

                check = 0
                for byte in byte_list:
                    check += byte
                
                if checksum != check:
                    print("Wrong data checksum {} at line {}.".format(hex(check), index + 1))
                    sys.exit()

                output_bytes += bytes(byte_list)

    outfile = open(args.output, "wb")
    outfile.write(output_bytes)

if __name__ == "__main__":
    main()