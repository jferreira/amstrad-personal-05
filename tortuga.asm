    org #a000

.la000
    call la35e
    call #b900  ; KL U ROM ENABLE
    
    ; Adapt the basic vectors depending on the machine
    push af
    ld a,(#c002)
    or a
    jr z,la03e  ; BASIC v1.0
    cp #01
    jr nz,la047 ; BASIC v1.1 6128
                ; Otherwise BASIC v1.1 664

.la012 equ $ + 1
.la011
    ld hl,#bd61 ; KONV HLA TO FLO
    ld (la26f),hl
    ld (la27e),hl
.la01b equ $ + 1
    ld hl,#bd94 ; FLO DEG/RAD
    ld (la274),hl
.la021 equ $ + 1
    ld hl,#bda9 ; FLO SIN
    ld (la297),hl
.la027 equ $ + 1
    ld hl,#bdac ; FLO COS
    ld (la29d),hl
.la02d equ $ + 1
    ld hl,#bd82 ; FLO MULT 
    ld (la2a3),hl
    ld (la2a9),hl
.la036 equ $ + 1
    ld hl,#bd67 ; ROUND FLO TO HLA
    ld (la2b9),hl
    ld (la2c2),hl

.la03e
    ld bc,rsx_table
    ld hl,workspace
    jp #bcd1    ; KL LOG EXT

.la047
    ld hl,#bd64
    ld (la012),hl
    ld hl,#bd97
    ld (la01b),hl
    ld hl,#bdac
    ld (la021),hl
    ld hl,#bdaf
    ld (la027),hl
    ld hl,#bd85
    ld (la02d),hl
    ld hl,#bd6a
    ld (la036),hl
    jr la011

rsx_table:
    defw name_table
    jp get_xy
    jp set_xy
    jp set_color
    jp get_dir
    jp set_dir
    jp move_forward
    jp move_backward
    jp turn_right
    jp turn_left
    jp draw_circle

workspace:
    defs 4

name_table:
    defb 'X','Y'+$80
    defb 'PONX','Y'+$80
    defb 'COLO','R'+$80
    defb 'RUMB','O'+$80
    defb 'PONRUMB','O'+$80
    defb 'ADELANT','E'+$80
    defb 'ATRA','S'+$80
    defb 'DERECH','A'+$80
    defb 'IZQUIERD','A'+$80
    defb 'CIRCUNFERENCI','A'+$80

workspace:
    defs 4

get_xy:
    cp #02
    jp nz,la352
    ld de,(la3d0)
    ld l,(ix+#02)
    ld h,(ix+#03)
    ld (hl),e
    inc hl
    ld (hl),d
    ld de,(la3d2)
    ld l,(ix+#00)
    ld h,(ix+#01)
    ld (hl),e
    inc hl
    ld (hl),d
    ret

set_xy:
    cp #02
    jp nz,la352
    ld l,(ix+#00)
    ld h,(ix+#01)
    ld (la3d2),hl
    ld l,(ix+#02)
    ld h,(ix+#03)
    ld (la3d0),hl
    ret

set_color:
    dec a
    jp nz,la352
    ld a,(ix+#00)
    ld (la3fc),a
    ret

get_dir:
    dec a
    jp nz,la352
    ld l,(ix+#00)
    ld h,(ix+#01)
    ld de,(la3fd)
    ld (hl),e
    inc hl
    ld (hl),d
    ret

set_dir:
    dec a
    jp nz,la352
    ld l,(ix+#00)
    ld h,(ix+#01)
    push hl
    ld de,#0168
    or a
    sbc hl,de
    pop hl
    jp nc,la358
    ld (la3fd),hl
    ret

move_forward:
    dec a
    jp nz,la352
    call la25b
    call la265
    call la271
    ld de,la3e8
    call la29f
    ld de,la3e8
    call la2a5
    call la2b5
    call la34c
    jp la2ab

.la169
    ld de,#0168
    add hl,de
    jr la187

move_backward:
    dec a
    jp nz,la352
    call la25b
    call la265
    ld hl,(la3fd)
    ld (la3ff),hl
    ld de,#00b4
    or a
    sbc hl,de
    jr c,la169

.la187
    ld (la3fd),hl
    call la271
    ld de,la3e8
    call la29f
    ld de,la3e8
    call la2a5
    call la2b5
    call la34c
    call la2ab
    ld hl,(la3ff)
    ld (la3fd),hl
    ret

turn_right:
    dec a
    jp nz,la352
    ld e,(ix+#00)
    ld d,(ix+#01)
    ld hl,(la3fd)
    add hl,de
    ld (la3fd),hl
    ld de,#0168
    or a
    sbc hl,de
    ret c
    ld (la3fd),hl
    ret

turn_left:
    dec a
    jp nz,la352
    ld e,(ix+#00)
    ld d,(ix+#01)
    ld hl,(la3fd)
    or a
    sbc hl,de
    ld (la3fd),hl
    ret nc
    ld de,#0168
    add hl,de
    ld (la3fd),hl
    ret

draw_circle:
    dec a
    jp nz,la352
    ld hl,(la3fd)
    ld (la405),hl
    ld hl,(la3d0)
    ld (la401),hl
    ld hl,(la3d2)
    ld (la403),hl
    call la34c
    call la265
    ld hl,#0000
    ld (la3fd),hl
    call la271
    ld de,la3e8
    call la29f
    ld de,la3e8
    call la2a5
    call la2b5
    call la25b
    ld hl,#0168

.la21b
    push hl
    ld (la3fd),hl
    ld hl,(la401)
    ld (la3d0),hl
    ld hl,(la403)
    ld (la3d2),hl
    call la271
    ld de,la3e8
    call la29f
    ld de,la3e8
    call la2a5
    call la2b5
    call la2ab
    pop hl
    ld de,#0005
    sbc hl,de
    jr nc,la21b
    ld hl,(la405)
    ld (la3fd),hl
    ld hl,(la401)
    ld (la3d0),hl
    ld hl,(la403)
    ld (la3d2),hl
    ret

.la25b
ld de,(la3d0)
ld hl,(la3d2)
jp #bbc0

.la265
ld l,(ix+#00)
ld h,(ix+#01)
ld de,la3e8
.la26f equ $ + 1
jp #bd40

.la271
ld a,#01
.la274 equ $ + 1
call #bd73
ld hl,(la3fd)
ld de,la3f7
push de
.la27e equ $ + 1
call #bd40
pop hl
push hl
ld de,la3ed
ld bc,#0005
ldir
pop hl
ld de,la3f2
ld bc,#0005
ldir
ld hl,la3ed
.la297 equ $ + 1
call #bd88
ld hl,la3f2
.la29d equ $ + 1
jp #bd8b

.la29f
ld hl,la3ed
.la2a3 equ $ + 1
jp #bd61
.la2a5
ld hl,la3f2
.la2a9 equ $ + 1
jp #bd61
.la2ab
ld de,(la3d0)
ld hl,(la3d2)
jp #bbf6
.la2b5
ld hl,la3ed
.la2b9 equ $ + 1
call #bd46
ld (la3d4),hl
ld hl,la3f2
.la2c2 equ $ + 1
call #bd46
ld (la3de),hl
ld hl,(la3fd)
push hl
ld de,#010e
or a
sbc hl,de
jr nc,la332
pop hl
push hl
ld de,#00b4
or a
sbc hl,de
jr nc,la316
pop hl
ld de,#005a
or a
sbc hl,de
jr nc,la2fd
ld hl,(la3d4)
ld de,(la3d0)
add hl,de
ld (la3d0),hl
ld hl,(la3de)
ld de,(la3d2)
add hl,de
ld (la3d2),hl
ret

.la2fd
ld hl,(la3d2)
ld de,(la3de)
or a
sbc hl,de
ld (la3d2),hl
ld hl,(la3d0)
ld de,(la3d4)
add hl,de
ld (la3d0),hl
ret

.la316
pop hl
ld hl,(la3d2)
ld de,(la3de)
or a
sbc hl,de
ld (la3d2),hl
ld hl,(la3d0)
ld de,(la3d4)
or a
sbc hl,de
ld (la3d0),hl
ret

.la332
pop hl
ld hl,(la3d0)
ld de,(la3d4)
or a
sbc hl,de
ld (la3d0),hl
ld hl,(la3d2)
ld de,(la3de)
add hl,de
ld (la3d2),hl
ret

.la34c
ld a,(la3fc)
jp #bbde
.la352
ld hl,la38d
jp la384
.la358
ld hl,la3a1
jp la384

.la35e
    ld a,#01
    call #bc0e  ; SCR SET MODE
    xor a
    ld bc,#0000
    call #bc32  ; SCR SET INK
    ld a,#01
    ld bc,#1a1a
    call #bc32  ; SCR SET INK
    ld bc,#0000
    call #bc38  ; SCR SET BORDER
    ld hl,la3b2
    call la384
    ld a,#c9
    ld (la000),a
    ret

.la384
    ld a,(hl)
    or a
    ret z
    call #bb5a  ; TXT OUTPUT
    inc hl
    jr la384

.la38d
    defb 'ERROR EN PARAMETROS',0

.la3a1
    defb 'RUMBO MUY GRANDE',0

.la3b2
    defb 'GRAFICOS DE TORTUGA ACTIVADOS',0

.la3d0
ld b,b
.la3d2 equ $ + 1
ld bc,#00c8

.la3d4
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop

.la3de
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop

.la3e8
nop
nop
nop
nop
nop

.la3ed
nop
nop
nop
nop
nop

.la3f2
nop
nop
nop
nop
nop

.la3f7
nop
nop
nop
nop
nop
.la3fd equ $ + 1
.la3fc
ld bc,#0000
.la3ff
nop
nop
.la401
nop
nop
.la403
nop
nop
.la405
nop
nop
nop
