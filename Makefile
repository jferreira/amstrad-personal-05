IDSK = /mnt/ext/Homebrew/machines/cpc/toolbox/idsk/build/iDSK

SRCS = $(wildcard *.bas)

.PHONY: all clean

all: amstrad_personal_05.dsk

amstrad_personal_05.dsk: $(SRCS)
	rm -f $@
	$(IDSK) $@ -n
	$(foreach SRC,$(SRCS), unix2dos $(SRC);)
	$(foreach SRC,$(SRCS), $(IDSK) $@ -i $(SRC) -t 0;)
	

clean:
	rm -f amstrad_personal_05.dsk

